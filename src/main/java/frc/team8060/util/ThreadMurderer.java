package frc.team8060.util;

/**
 * <h1>🔪</h1>
 */
public interface ThreadMurderer {
    /**
     * Interrupts a thread, then forcibly stops
     * it if it is still alive after 3 seconds.
     * @param victim The victim thread.
     */
    @SuppressWarnings("deprecation")
    public static void murderThread(Thread victim) {
        if(!victim.isAlive()) {
            // already dead
            return;
        }

        victim.interrupt();
        int numSleeps = 0;
        try {
            while(victim.isAlive() && numSleeps < 30) {
                Thread.sleep(100);
                numSleeps++;
            }
        } catch (InterruptedException ignored) {}
        if(victim.isAlive()) {
            victim.stop();
        }
    }
}
