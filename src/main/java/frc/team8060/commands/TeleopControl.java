package frc.team8060.commands;

import edu.wpi.first.wpilibj.Joystick;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.team8060.subsystems.Drivetrain;

public class TeleopControl extends CommandBase {

    private final Drivetrain drivetrain;
    private final Joystick joystick;

    public TeleopControl(Drivetrain drivetrain, Joystick joystick) {
        this.drivetrain = drivetrain;
        this.joystick = joystick;
        addRequirements(this.drivetrain);
    }

    @Override
    public void execute() {
        double leftX = joystick.getRawAxis(0); // left stick horizontal axis
        double leftY = joystick.getRawAxis(1); // right stick vertical axis
        if (Math.abs(leftY) < 0.1) {
            leftY = 0;
        }
        if (Math.abs(leftX) < 0.1) {
            leftX = 0;
        }
        // NOTE : left Y is possible reversed already
        drivetrain.arcadeDrive(leftY, leftX);

        /*
            Grant's Holy Algorithm
            2021-2022
            RIP

                if (Math.abs(leftX) >= 0.05 || Math.abs(leftY) >= 0.05) {
                // Grant's Holy Algorithm of Holiness
                // desmos.com/calculator/2nqgbfgzp5
                drivetrain.tankDrive(
                        Math.sin(leftX + leftY) * POWER_MULTIPLIER,
                        Math.sin(leftX - leftY) * POWER_MULTIPLIER
                );
                }
        */
    }
}
