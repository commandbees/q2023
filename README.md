# Team 8060 2023 Code #

## Stuff Tried This Year ##
* Vision Using raspberry pi
  * Network tables
  * Apriltags
  * Coprocessor
* Using encoders
  * 3 Total
    * 1 for each side of drive train (2)
    * 1 for the arm
* Switch from PWM to CAN for connecting motor controllers
  * REVRobotics Java api had to be added for spark maxes
* Some Kotlin because why not :)
* Ramsete Commands were understood, but never got to be used
    * Some helper methods exist in the code for them. They are untested though
