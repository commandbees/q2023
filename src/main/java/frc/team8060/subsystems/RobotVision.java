package frc.team8060.subsystems;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.cscore.VideoSource;
import edu.wpi.first.vision.VisionPipeline;
import edu.wpi.first.vision.VisionThread;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import org.opencv.core.Mat;

public class RobotVision extends SubsystemBase {

    private VideoSource camera;

    public RobotVision() {
        this.camera = CameraServer.startAutomaticCapture();

        VisionThread visionThread = new VisionThread(camera, new MyVisionPipeline(), a -> {

        });
        visionThread.start();
    }

    class MyVisionPipeline implements VisionPipeline {

        @Override
        public void process(Mat image) {

        }
    }
}
