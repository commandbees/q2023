package frc.team8060.constants;

public class ArmConstants {

    // CURRENTLY ALL VALUES HERE ARE FILLER

    public static final int INTAKE_MOTOR_PORT = 6;
    public static final int ARM_MOTOR_PORT = 7; // switch this to 7 later

    public static final int ARM_RAISE_BUTTON = 3;
    public static final int ARM_LOWER_BUTTON = 4;


    // not being used for now
    // public static final int ARM_E_BRAKE= 5;

    //Edgar: This is for the upper arm motor (I commented it out for now)
    public static final int ARM_ENCODER_RESET_BUTTON = 8;
    public static final int INTAKE_IN_BUTTON = 5;
    public static final int INTAKE_OUT_BUTTON = 6;

    public static final boolean ENCODER_INVERTED = false;

    // this is to just keeps positive and negative values doing what
    // it's assumed they will do
    // KEEP THIS TRUE
    public static final boolean ARM_REVERSED = true;
    public static final boolean INTAKE_REVERSED = false;
    // same logic as above

    public static final double ARM_KG = 0.5;
    public static final double ARM_KP = 0.5;
    public static final double ARM_KV = 0.5;
    public static final double ARM_KA = 0.5;

}
